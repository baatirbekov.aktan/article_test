<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('store', 'destroy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $like = Like::where('user_id', Auth::id())->where('article_id', $request->get('article_id'))->first();

        if ($like) {
            $like->delete();
        } else {
            $like = new Like();
            $like->article_id = $request->get('article_id');
            $like->user_id = $request->user()->id;
            $like->save();
        }

        return response()->json(['status' => 'done'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Like $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like)
    {
        $like->delete();
        return response()->noContent();
    }
}
