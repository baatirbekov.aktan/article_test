<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $query = $request->validate([
            'page' => ['integer', 'nullable'],
            'per_page' => ['integer', 'nullable']
        ]);
        $per_page = 10;
        if (array_key_exists('per_page', $query)) {
            $per_page = $query['per_page'];
        }

        $articles = Article::paginate($per_page);
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleRequest $request
     * @return ArticleResource
     */
    public function store(ArticleRequest $request): ArticleResource
    {
        $data = $request->validated();
        $article = new Article($data);
        $article->user_id = $request->user()->id;
        $article->save();
        return new ArticleResource($article);
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article): ArticleResource
    {
        return new ArticleResource($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleRequest $request
     * @param Article $article
     * @return ArticleResource
     */
    public function update(ArticleRequest $request, Article $article): ArticleResource
    {

        $data = $request->all();
        $article->update($data);
        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article): Response
    {
        $article->delete();
        return response()->noContent();
    }
}
