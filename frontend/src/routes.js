import Home from "./components/common/Home";
import Articles from "./components/articles/ArticleList";
import CreateArticle from "./components/articles/CreateArticle";
import ShowArticle from "./components/articles/ShowArticle";
import EditArticle from "./components/articles/EditArticle";


const routes = [
    {
        path: '/',
        name: 'Home',
        element: <Home/>,
        auth: false,
        includeNav: true,
    },
    {
        path: '/articles',
        name: 'Articles',
        element: <Articles/>,
        auth: true,
        includeNav: true,
    },
    {
        path: '/articles/create',
        name: 'Create article',
        element: <CreateArticle/>,
        auth: true,
        includeNav: true,
    },
    {
        path: '/articles/:articleId',
        name: 'Article',
        element: <ShowArticle/>,
        auth: true,
        includeNav: false,
    },
    {
        path: '/articles/edit/:articleId',
        name: 'Article edit',
        element: <EditArticle/>,
        auth: true,
        includeNav: false,
    }
];

export default routes;
