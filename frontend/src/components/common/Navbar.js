import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import {ThemeProvider, createTheme} from '@mui/material/styles';
import {Link as RouterLink} from "react-router-dom";
import {isAuth} from "../../utils";


const ResponsiveAppBar = ({routes}) => {
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleLogout = () => {
        localStorage.removeItem('token');
        handleCloseUserMenu();
        window.location.href = '/';
    }

    const navLinks = () => {
        let links;
        links = [];
        routes.map((route, key) => {
            if (route.includeNav) {
                if (route.auth) {
                    if (isAuth()) {
                        links.push(
                            <MenuItem key={key} onClick={handleCloseNavMenu}>
                                <Typography textAlign="center">
                                    <Button component={RouterLink} to={route.path} variant="contained">
                                        {route.name}
                                    </Button>
                                </Typography>
                            </MenuItem>
                        )
                    }
                } else {
                    links.push(
                        <MenuItem key={key} onClick={handleCloseNavMenu}>
                            <Typography textAlign="center">
                                <Button component={RouterLink} to={route.path} variant="contained">
                                    {route.name}
                                </Button>
                            </Typography>
                        </MenuItem>
                    )
                }
            }
        })
        return links;
    }

    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: '#1976d2',
            },
        },
    });

    const navButtons = () => {
        let buttons;
        buttons = [];
        routes.map((route, key) => {
            if (route.includeNav) {
                if (route.auth) {
                    if (isAuth()) {
                        buttons.push(
                            <Button
                                key={key}
                                onClick={handleCloseNavMenu}
                                sx={{my: 2, color: 'white', display: 'block'}}
                            >
                                <Button component={RouterLink} to={route.path} variant="contained">
                                    {route.name}
                                </Button>
                            </Button>
                        )
                    }
                } else {
                    buttons.push(
                        <Button
                            key={key}
                            onClick={handleCloseNavMenu}
                            sx={{my: 2, color: 'white', display: 'block'}}
                        >
                            <Button component={RouterLink} to={route.path} variant="contained">
                                {route.name}
                            </Button>
                        </Button>
                    )
                }
            }
        })
        return buttons;
    };

    const authButtons = () => {
        const token = isAuth();
        if (token) {
            return <Box sx={{flexGrow: 0}}>
                <Tooltip title="Open settings">
                    <IconButton onClick={handleOpenUserMenu} sx={{p: 0}}>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg"/>
                    </IconButton>
                </Tooltip>
                <Menu
                    sx={{mt: '45px'}}
                    id="menu-appbar"
                    anchorEl={anchorElUser}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={Boolean(anchorElUser)}
                    onClose={handleCloseUserMenu}
                >

                    <>
                        <MenuItem onClick={handleLogout}>
                            <Typography textAlign="center">Logout</Typography>
                        </MenuItem>
                    </>
                </Menu>
            </Box>
        } else {
            return <Box sx={{flexGrow: 0}}>
                <Button component={RouterLink} to="/auth/login" variant="contained">
                    Login
                </Button>
                <Button component={RouterLink} to="/auth/register" variant="contained">
                    Register
                </Button>
            </Box>;
        }
    }

    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <Box sx={{flexGrow: 1, display: {xs: 'flex', md: 'none'}}}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="inherit"
                            >
                                <MenuIcon/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: {xs: 'block', md: 'none'},
                                }}
                            >
                                {navLinks()}
                            </Menu>
                        </Box>
                        <Box sx={{flexGrow: 1, display: {xs: 'none', md: 'flex'}}}>
                            {navButtons()}
                        </Box>
                        {authButtons()}
                    </Toolbar>
                </Container>
            </AppBar>
        </ThemeProvider>
    );
};
export default ResponsiveAppBar;
