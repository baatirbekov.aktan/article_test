<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Запустить

- cd database
- touch database.sqlite
- cd ../
- cp .env.example .env
- docker-compose build
- docker-compose up -d
- docker exec -it app bash
- php artisan migrate:refresh --seed
- php artisan passport:install
- chmod 777 -R storage/
- chmod 777 -R database/

http://localhost:3000/
